import { BASE_DIR } from '../../utils/consts';
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    content: '',
    showOneButtonDialog: false,
    oneButton: [{text: '关闭'}],
  },

  contentChange: function (e) {
    this.setData({
      content: e.detail.value.substring(0, 200)
    });
  },
  push_feedback: function (e) {
    if(!this.data.content) return
    let self = this;
    wx.request({
      url: BASE_DIR + '/feedback/',
      method: 'POST',
      header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
      data: {'content': self.data.content},
      success: function (res){
        self.setData({
          content: '',
          showOneButtonDialog: true,
        })
      }
    })
  },
  tapDialogButton(e) {
    this.setData({
        showOneButtonDialog: false
    })
},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})