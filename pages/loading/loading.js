import { BASE_DIR } from '../../utils/consts';

const app = getApp();


const images = [
  'dts.jpeg',
  'qlmg.jpeg',
  'qtbj.jpeg',
  'zpzq.jpeg',
];

Page({

  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    image: 'qlmg.jpeg',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let index = Math.floor(Math.random() * images.length);
    let self = this;
    wx.getSystemInfo( {
        success: function( res ) {
            self.setData( {
                winWidth: res.windowWidth,
                winHeight: res.windowHeight,
                image: images[index]
            });
        }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.login({
      success: res => {
        app.globalData.code = res.code
        console.log('res.code', res.code)
        wx.request({
          url: BASE_DIR + '/login/?code=' + res.code,
          success: function (resp){
            console.log('resp.data.data', resp.data.data);
            app.globalData.userInfo = resp.data.data;
            wx.switchTab({url: '/pages/index/index'});
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})