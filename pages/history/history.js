import { BASE_DIR } from '../../utils/consts';

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    reports: [],
    page: 1,
    slideButtons: [{
      type: 'warn',
      text: '删除',
      extClass: 'test',
      src: '/images/icon_del.svg'
    }],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let self = this;
    if (app.globalData.userInfo.openid){
      wx.request({
        url: BASE_DIR + '/reports/',
        header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
        success: function (res){
          self.setData({
            reports: res.data.data,
            page: 1,
          })
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let self = this;
    wx.request({
      url: BASE_DIR + '/reports/',
      header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
      success: function (res){
        self.setData({
          reports: res.data.data,
          page: 1,
        })
      }
    })
  },

  slideButtonTap(e) {
    let self = this;
    let rpt_id = e.target.dataset.id;
    let index = e.target.dataset.index;
    wx.request({
      url: BASE_DIR + '/report/' + rpt_id + '/delete/',
      header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
      success: function (res){
        let rpts = self.data.reports
        rpts.splice(index, 1)
        self.setData({
          reports: rpts
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    refresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let self = this;
    if (app.globalData.userInfo.openid){
      wx.request({
        url: BASE_DIR + '/reports/?page=' + (self.data.page+1),
        header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
        success: function (res){
          self.setData({
            reports: [...self.data.reports, ...res.data.data],
            page: self.data.page + 1
          })
        }
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})