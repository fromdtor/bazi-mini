import { BASE_DIR } from '../../utils/consts';

const app = getApp();


Page({
  onShareAppMessage() {
    return {
      title: 'tabs',
      path: 'page/weui/example/tabs/tabs'
    }
  },
  data: {
    tabs: [],
    activeTab: 0,
  },

  onLoad(options) {
    console.log('options', options)
    let self = this
    wx.request({
      url: BASE_DIR + '/report/' + options.rpt_id + '/explanation/',
      header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
      success: function (res){
        self.setData({
          tabs: res.data.data,
          rpt_id: options.rpt_id,
        })
      }
    })
  },

  onTabClick(e) {
    const index = e.detail.index
    this.setData({ 
      activeTab: index 
    })
  },

  onChange(e) {
    const index = e.detail.index
    this.setData({ 
      activeTab: index 
    })
  }
})
