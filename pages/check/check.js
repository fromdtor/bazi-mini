import { BASE_DIR } from '../../utils/consts';

const app = getApp();

Page({
  data: {
      winWidth: 0,
      winHeight: 0,
      currentTab: 0,
      name: '',
      date: "2000-01-01",
      time: "12:01",
      genders: ["男", "女"],
      gender: "男",
      date_type: '阳历',
      date_types: ['阳历', '农历'],
      gz: [
        ['甲', '乙', '丙', '丁', '戊', '己', '庚', '辛', '壬', '癸'],
        ['子', '丑', '寅', '卯', '辰', '巳', '午', '未', '申', '酉', '戌', '亥'],
      ],
      year: ['甲', '子'],
      month: ['乙', '丑'],
      day: ['丙', '寅'],
      hour: ['丁', '卯'],
  },
  onLoad: function() {
      console.log('app.globalData', app.globalData);

      var that = this;
    
      /**
       * 获取当前设备的宽高
       */
      wx.getSystemInfo( {

          success: function( res ) {
              that.setData( {
                  winWidth: res.windowWidth,
                  winHeight: res.windowHeight
              });
          }

      });
  },
    
//  tab切换逻辑
  swichNav: function( e ) {

      var that = this;

      if( this.data.currentTab === e.target.dataset.current ) {
          return false;
      } else {
          that.setData( {
              currentTab: e.target.dataset.current
          })
      }
  },
  genderChange: function (e) {
    this.setData({
        gender: this.data.genders[e.detail.value]
    })
  },
  dateTypeChange: function (e) {
    this.setData({
      date_type: this.data.date_types[e.detail.value]
    })
  },
  nameChange: function (e) {
    this.setData({
        name: e.detail.value
    });
  },
  bindDateChange: function (e) {
    this.setData({
        date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
        time: e.detail.value
    })
  },
  bindChange: function( e ) {

      var that = this;
      that.setData( { currentTab: e.detail.current });

  },
  ymdhChange: function (e) {
    let field = e.target.dataset.field;
    let indexes = e.detail.value;
    this.setData({
      [field]: [this.data.gz[0][indexes[0]], this.data.gz[1][indexes[1]]]
    });
  },
  gen_report: function (e) {
    // 输入生日
    let data = this.data;
    let api = [
      BASE_DIR + '/gen_report/?gender=' + data.gender,
      "date=" + data.date,
      "time=" + data.time,
      'name=' + data.name,
      'date_type=' + data.date_type,
    ].join('&');
    wx.request({
      url: api,
      header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
      success: function (res){
        let next_page = "/pages/pan/pan?id=" + res.data.id;
        wx.navigateTo({url: next_page})
      }
    })
  },
  gen_report2: function (e) {
    // 输入八字
    let data = this.data;
    let api = [
      BASE_DIR + '/gen_report2/?gender=' + data.gender,
      'name=' + data.name,
      'year=' + data.year.join(''),
      'month=' + data.month.join(''),
      'day=' + data.day.join(''),
      'hour=' + data.hour.join(''),
    ].join('&');
    wx.request({
      url: api,
      header: {'Authorization': 'bazi '+ app.globalData.userInfo.openid},
      success: function (res){
        let next_page = "/pages/pan/pan?id=" + res.data.id;
        wx.navigateTo({url: next_page})
      }
    })
  }
})